# Proof platform
![Logo](logo.png)<br/>
#### Authors: Bc. Tomáš Kocman, Ing. Libor Polčák, Ph.D.<br/>
This platform enables scraping of web page content and storing the content in offline 
database. The web crawl is performed using user-supplied regular expressions that may 
represent for example Torrent file names, Bitcoin wallets or keywords. Collected data 
may be used for law enforcement and other entitites, such as searching for information 
about a specific product. Archived data are stored in a database and available for 
later use without the possibility of modification due to web server updates.<br/>

This is the recommended way of using this platform.

### Steps to run Proof Platform using Docker
```
> edit docker-compose.yml field <volumes> according to your needs
> docker-compose build
> docker-compose up -d
```

This will build five Docker images (Redis, Postgres, Scrapy, Lemmiwinks, pgAdmin4). All images will be launched as containers.

### Acknowledgments

This work was supported by the Ministry of the Interior of the Czech Republic grant
number VI20172020062.